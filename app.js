/**
 * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */

import * as http from 'http';
import fetch from 'node-fetch';

http.createServer(function (request, response) {

    if (request.method === "GET") {
        switch (request.url) {
            case `/home`:
            case `/`:
                response.write("<h2>Home</h2>");
                break;
            case `/about`:
                response.write("<h2>About</h2>");
                break;
            case `/contact`:
                response.write("<h2>Contacts</h2>");
                break;
            default:
                response.write("<h2>Not found</h2>");
        }
        response.end();

    } else if (request.method === "POST") {
        let body = ``;
        request.on(`data`, chunk => {
            body += chunk.toString();
        });
        request.on(`end`, () => {
            body = {...JSON.parse(body),
            returned: true}
            response.end(JSON.stringify(body));
        })

    } else {
        response.end(`Данный запрос не поддерживается сервером`);
    }


}).listen(3000);


async function sendEcho(message) {
    const result = await fetch(`http://localhost:3000`, {
        method: `POST`,
        headers: {
            'Content-Type': 'application/json'

        },

        body: JSON.stringify(message)
    })
    .then(response => response.json());
    console.log(result)
}

sendEcho({message: "Привет сервис, я жду от тебя ответа"});