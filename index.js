import fetch from 'node-fetch';

/**
 * Задание 1 - Имитируем работу с сервером - Promise
 *
 * 1. Написать функцию getList, которая возвращает Promise с данными о списке задач, иммитируя
 * задержку перед получением в 2 секунды:
 * [
 * { id: 1, title: 'Task 1', isDone: false },
 * { id: 2, title: 'Task 2', isDone: true },
 * ]
 * 2. Написать скрипт который получит данные из функции getList и выведет на экран список задач.
 * 3. Изменить промис так, чтобы он возвращал ошибку
 * 4. Дополнить скрипт так, чтобы если промис возвращает ошибку выводилось сообщение об ошибке
 */
const getList = (ms) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve([
            {id: 1, title: 'Task 1', isDone: false},
            {id: 2, title: 'Task 2', isDone: true},
        ]), ms)
        // setTimeout(() => reject(new Error(`ошибка!`)), ms);
    })
}
// getList(2000).then((result) => console.log(result))
//     .catch((error) => console.log(error));

/**
 * Задание 2 - Чейнинг (цепочки) промисов
 *
 * Написать функцию которая будет соберет строку "Я использую цепочки обещаний", конкотенируя каждое
 * слово через отдельный then блок с задержкой в 1 секунду на каждой итерации.
 * Результат вывести в консоль.
 */

const promiseChain = (ms) => {
    new Promise(resolve => setTimeout(() => resolve(`Я`), ms))
        .then(firstResult => {
            return new Promise(resolve2 => setTimeout(() => resolve2(firstResult + ` использую`), ms))
        })
        .then(firstResult => {
            return new Promise(resolve3 => setTimeout(() => resolve3(firstResult + ` цепочки`), ms))
        })
        .then(firstResult => {
            return new Promise(resolve4 => setTimeout(() => resolve4(firstResult + ` обещаний`), ms))

        })
        .then(result => console.log(result));
};

// promiseChain(1000);
/**
 * Задание 3 - Параллельные обещания
 *
 * Написать функцию которая будет соберет строку "Я использую вызов обещаний параллельно",
 * используя функцию Promise.all(). Укажите следующее время задержки для каждого
 * промиса возвращаего слова:
 * Я - 1000,
 * использую - 800
 * вызов - 1200
 * обещаний - 700
 * параллельно - 500
 * Результат вывести в консоль.
 */
// Promise.all([
//     delay(1000, `Я`),
//     delay(800, ` использую`),
//     delay(1200, ` вызов`),
//     delay(700, ` обещаний`),
//     delay(500, ` параллельно`),
// ]).then(results => console.log(results.join()))
/**
 * Задание 4 - Напишите функцию delay(ms), которая возвращает промис,
 * переходящий в состояние "resolved" через ms миллисекунд.
 *
 * delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'))
 */

function delay(ms, value) {
    return new Promise(resolve => {
        setTimeout(() => resolve(value), ms)
    })
}

// delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'));

/**
 * Задание 5 - Решите 3 задачу, используя, функцию delay
 */

/**
 * Задание 6 - Напишите функцию, которая загрузит данные по первому фильму в котором встретилась планета Татуин, используя
 * предоставленный API (https://swapi.dev)
 */

async function getFilmName(planetName) {

    const films = await fetch('https://swapi.dev/api/films')
        .then(res => res.json())
        .then(data => data.results);

    for (let i = 0; i < films.length; i++) {

        const planetsData = await Promise.all(
            films[i].planets.map(planetUrl => fetch(planetUrl).then(res => res.json()))
        );
        if (planetsData.find(planet => planet.name === planetName)) {
            return films[i].title;
        }
    }
    return 'Фильм не найден';

}

getFilmName('Tatooine').then(filmName => console.log(filmName));


/**
 * Задание 7 - Напишите функцию, которая выведет название транспортного средства на котором впервые ехал Anakin Skywalker, используя
 * предоставленный API (https://swapi.dev)
 */

async function getTransportByName(characterName) {
    const data = await fetch(`https://swapi.dev/api/people/`).then(res => res.json());
    const allCharacters = data.results;
    const targetCharacter = allCharacters.find(person => person.name === characterName);
    await fetch(targetCharacter.vehicles[0])
        .then(res => res.json())
        .then(vehicle => console.log(vehicle.name));
}

// getTransportByName('Luke Skywalker');

